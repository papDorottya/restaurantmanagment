import presentationLayer.MainGraphicalUserInterface;
import services.ResourceService;

public class App {

    public static void main(String args[]) {
        ResourceService.getInstance().setFileName(args[0]);

        new MainGraphicalUserInterface();
    }
}
