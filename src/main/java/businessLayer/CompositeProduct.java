package businessLayer;

import java.util.ArrayList;
import java.util.List;

public class CompositeProduct extends MenuItem{
    private List<CompositeProduct> compositeProducts = new ArrayList<>();

    public CompositeProduct(int price, String name) {
        super(price, name);
    }

    public void setCompositeProducts(List<CompositeProduct> compositeProducts) {
        this.compositeProducts = compositeProducts;
    }

    public List<CompositeProduct> getCompositeProducts() {
        return compositeProducts;
    }
}
