package businessLayer;

import java.awt.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public interface IRestaurantProcessing {
    /**
     * @param menuItem to be added in menu
     * @pre menuItem != null
     * @pre menuItems.contains(menuItem) == false;
     * @post size() == size()@pre + 1
     */
    void createMenuItem(MenuItem menuItem);

    /**
     * @param menuItem to be deleted from menu
     * @pre menuItem != null
     * @pre menuItems.contains(menuItem) == true;
     * @post size() == size()@pre - 1
     */
    void deleteMenuItem(MenuItem menuItem);

    /**
     * @param old to be updated
     * @param newMenuItem to replace the old one
     * @pre old != null && newMenuItem != null
     * @pre menuItems.contains(old) == true
     * @post size() == size()@pre
     */
    void editMenuItem(MenuItem old, MenuItem newMenuItem);

    /**
     * @param table id of the table
     * @param orderedItem ordered menu item
     * @pre table > 0
     * @pre orderedItem != null
     * @post size() == size()@pre + 1
     */
    void createNewOrder(int table, MenuItem orderedItem);

    /**
     * @param table id of the table
     * @pre table > 0
     */
    void computeBill(int table);

    /**
     * @pre menuItems.contains(null) == false
     */
    boolean isWellFormed();
}
