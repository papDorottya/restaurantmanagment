package businessLayer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MenuItem implements Serializable {
    private int price;
    private String name;

    public MenuItem(int price, String name) {
        this.price = price;
        this.name = name;
    }

    public int computePrice() {
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        MenuItem menuItem = (MenuItem) o;
        return this.name.equalsIgnoreCase(menuItem.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return this.name + " " + this.price;
    }

    public int getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }
}
