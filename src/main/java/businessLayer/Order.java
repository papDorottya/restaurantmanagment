package businessLayer;

import java.rmi.server.UID;
import java.util.Date;
import java.util.Objects;

public class Order {
    public String orderID;
    public Date date;
    public int table;

    public Order(int table) {
        this.orderID = String.valueOf(table);
        this.date = new Date();
        this.table = table;
    }

    public String getOrderID() {
        return orderID;
    }

    public Date getDate() {
        return date;
    }

    public int getTable() {
        return table;
    }

    public void setTable(int table) {
        this.table = table;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return orderID == order.orderID &&
                table == order.table &&
                date.equals(order.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderID, date, table);
    }
}
