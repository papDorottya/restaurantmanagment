package businessLayer;

import dataLayer.FileWriter;
import dataLayer.RestaurantSerializator;
import services.ResourceService;

import java.util.*;


public class Restaurant extends Observable implements IRestaurantProcessing {
    private static Restaurant singleInstance = null;
    RestaurantSerializator restaurantSerializator = new RestaurantSerializator(ResourceService.getInstance().getFileName());
    private HashMap<Order, List<MenuItem>> orders;
    private List<MenuItem> restaurantAvailableMenuItems;

    private Restaurant() {
        this.orders = new HashMap<>();
        this.restaurantAvailableMenuItems = this.restaurantSerializator.read();
    }

    public static Restaurant getInstance() {
        if(singleInstance == null) {
            singleInstance = new Restaurant();
        }

        return  singleInstance;
    }

    @Override
    public void createMenuItem(MenuItem menuItem) {
        assert menuItem != null;
        assert !restaurantAvailableMenuItems.contains(menuItem);
        int size = restaurantAvailableMenuItems.size();

        restaurantAvailableMenuItems.add(menuItem);
        restaurantSerializator.write(restaurantAvailableMenuItems);

        assert restaurantAvailableMenuItems.size() == size + 1;
        assert isWellFormed();
    }

    @Override
    public void deleteMenuItem(MenuItem menuItem) {
        assert menuItem != null;
        assert this.restaurantAvailableMenuItems.contains(menuItem);

        int size = restaurantAvailableMenuItems.size();

        this.restaurantAvailableMenuItems.remove(menuItem);
        this.restaurantSerializator.write(this.restaurantAvailableMenuItems);
        assert restaurantAvailableMenuItems.size() == size - 1;

        assert isWellFormed();
    }

    @Override
    public void editMenuItem(MenuItem old, MenuItem newMenuItem) {
        assert old != null && newMenuItem != null;
        assert restaurantAvailableMenuItems.contains(old);
        int size = restaurantAvailableMenuItems.size();

        this.restaurantAvailableMenuItems.remove(old);
        this.restaurantAvailableMenuItems.add(newMenuItem);
        restaurantSerializator.write(restaurantAvailableMenuItems);

        assert restaurantAvailableMenuItems.size() == size;
        assert isWellFormed();
    }

    @Override
    public void createNewOrder(int table, MenuItem orderedItem) {
        assert table > 0;
        assert orderedItem != null;
        int size = orders.keySet().size();

        Order order = findTable(table);

        if (order != null) {
            List<MenuItem> existingList = (List<MenuItem>) this.orders.get(order);
            existingList.add(orderedItem);
            this.orders.put(order, existingList);
        } else {
            List<MenuItem> newList = new ArrayList<>();
            newList.add(orderedItem);

            Order newOrder = new Order(table);
            this.orders.put(newOrder, newList);
            assert orders.keySet().size() == size + 1;
        }

        //notify chef
        setChanged();
        notifyObservers(orderedItem);

        assert isWellFormed();
    }

    public Order findTable(int table) {
        List<Order> existingOrders = new ArrayList<>(this.orders.keySet());

        for (Order order : existingOrders) {
            if (order.getTable() == table) {
                return order;
            }
        }

        return null;
    }

    @Override
    public void computeBill(int table) {
        assert table > 0;

        Order order = findTable(table);
        List<MenuItem> menuItemList = (List<MenuItem>) orders.get(order);

        new FileWriter(table, menuItemList).write();
    }

    @Override
    public boolean isWellFormed() {
        assert !restaurantAvailableMenuItems.contains(null);
        return !restaurantAvailableMenuItems.contains(null);
    }

    public String computeTotalPrice(int tableNumber) {
        int totalPrice = 0;
        Order order = findTable(tableNumber);
        List<MenuItem> menuItemList = (List<MenuItem>) orders.get(order);
        for (MenuItem mi : menuItemList) {
            totalPrice += mi.getPrice();
        }
        return String.valueOf(totalPrice);
    }

    public List<MenuItem> getTableMenuItems(int table) {
        Order order = findTable(table);
        List<MenuItem> menuItemList = (List<MenuItem>) orders.get(order);
        return menuItemList;
    }

    public List<MenuItem> getRestaurantAvailableMenuItems() {
        return restaurantAvailableMenuItems;
    }

    public List<MenuItem> getOrdersList() {
        List<MenuItem> orderList = new ArrayList<>();
        for (List<MenuItem> list : this.orders.values()) {
            for (MenuItem mi : list) {
                orderList.add(mi);
            }
        }
        return orderList;
    }
}

