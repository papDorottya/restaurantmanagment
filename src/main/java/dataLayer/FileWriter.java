package dataLayer;

import businessLayer.MenuItem;

import java.awt.*;
import java.io.File;
import java.util.Date;
import java.util.List;

public class FileWriter {
    private File file;
    private List<MenuItem> items;
    private int table;

    public FileWriter(int table, List<MenuItem> items) {
        file = new File("bill" + table + ".txt");
        System.out.println(file);
        createFile();

        this.table = table;
        this.items = items;
    }

    private void createFile() {
        try {
            file.createNewFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void write() {
        int sum = 0;
        StringBuilder sb = new StringBuilder();
        java.io.FileWriter writer;
        try {
            writer = new java.io.FileWriter(this.file);

            sb.append(new Date().toString() + "\n");
            sb.append("Bill Table " + this.table + "\n");
            sb.append("\nProducts:");
            for (MenuItem mi : this.items) {
                sb.append("\n" + mi.toString());
                sum += mi.getPrice();
            }

            sb.append("\n\nTOTAL SUM = " + sum);

            writer.write(sb.toString());
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
