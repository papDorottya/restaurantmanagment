package dataLayer;

import businessLayer.MenuItem;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class RestaurantSerializator {
    private String fileName;

    public RestaurantSerializator(String filename) {
        this.fileName = filename;
    }

    public List<MenuItem> read() {
        List<MenuItem> menuItems = new ArrayList<>();

        try {
            FileInputStream file = new FileInputStream(fileName);
            ObjectInputStream objectInputStream = new ObjectInputStream(file);
            menuItems = (List<MenuItem>) objectInputStream.readObject();

            objectInputStream.close();
            file.close();
            System.out.println("Object has been deserialized");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return menuItems;
    }
    public void write(List<MenuItem> menuItems) {
        try {
            FileOutputStream file = new FileOutputStream(fileName);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(file);
            objectOutputStream.writeObject(menuItems);

            objectOutputStream.close();
            file.close();
            System.out.println("Object has been serialized");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
