package presentationLayer;

import businessLayer.*;
import businessLayer.MenuItem;
import dataLayer.RestaurantSerializator;
import services.ResourceService;
import services.TableService;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AdministratorGraphicalUserInterface implements IRestaurantProcessing {
    private JLabel nameLabel;
    private JTextField nameText;

    private JLabel priceLabel;
    private JTextField priceText;

    private JButton compositeButton;
    private JButton baseButton;
    private JButton editButton;
    private JButton deleteButton;

    private JTable table;
    private DefaultTableModel tableModel;

    private JFrame frame;

    private RestaurantSerializator restaurantSerializator = new RestaurantSerializator(ResourceService.getInstance().getFileName());

    public AdministratorGraphicalUserInterface() {
        this.frame = new JFrame("Administrator");

        // create control panel
        JPanel controlsPanel = new JPanel();
        controlsPanel.setLayout(new GridLayout(3, 1));
        //add name input
        JPanel namePanel = new JPanel();
        namePanel.setLayout(new GridLayout(1, 2));
        this.nameLabel = new JLabel("Name");
        this.nameText = new JTextField("");
        namePanel.add(this.nameLabel);
        namePanel.add(this.nameText);
        controlsPanel.add(namePanel);
        //add price input
        JPanel pricePanel = new JPanel();
        pricePanel.setLayout(new GridLayout(1, 2));
        this.priceLabel = new JLabel("Price");
        this.priceText = new JTextField("");
        pricePanel.add(this.priceLabel);
        pricePanel.add(this.priceText);
        controlsPanel.add(pricePanel);
        //add buttons
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(4, 1));
        this.compositeButton = new JButton("Add Composite Product");
        this.baseButton = new JButton("Add Base Product");
        this.editButton = new JButton("Edit Product");
        this.deleteButton = new JButton("Delete Product");
        buttonsPanel.add(this.compositeButton);
        buttonsPanel.add(this.baseButton);
        buttonsPanel.add(this.editButton);
        buttonsPanel.add(this.deleteButton);
        controlsPanel.add(buttonsPanel);

        //create table panel
        JPanel tablePanel = new JPanel();
        tablePanel.setLayout(new GridLayout(1, 1));
        this.table = new JTable();
        this.tableModel = new DefaultTableModel();
        tableModel.addColumn("Name");
        tableModel.addColumn("Price");
        TableService.populateTable(this.tableModel, Restaurant.getInstance().getRestaurantAvailableMenuItems());
        this.table.setModel(tableModel);
        JScrollPane scrollTable = new JScrollPane(this.table);
        scrollTable.setLayout(new ScrollPaneLayout());
        scrollTable.setVisible(true);
        tablePanel.add(scrollTable);

        // add panels to main panel
        Container mainPanel = frame.getContentPane();
        mainPanel.setLayout(new GridLayout(1, 2));
        mainPanel.add(controlsPanel);
        mainPanel.add(tablePanel);
        frame.setSize(700, 700);
        frame.pack();

        this.registerActionListeners();
    }

    private void registerActionListeners() {
        final AdministratorGraphicalUserInterface that = this;
        this.compositeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int price = Integer.parseInt(that.priceText.getText());
                String name = nameText.getText();
                MenuItem menuItem = new CompositeProduct(price, name);
                that.createMenuItem(menuItem);
            }
        });

        this.baseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int price = Integer.parseInt(that.priceText.getText());
                String name = nameText.getText();
                MenuItem menuItem = new BaseProduct(price, name);
                that.createMenuItem(menuItem);
            }
        });

        this.editButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int price = Integer.parseInt(that.priceText.getText());
                String name = nameText.getText();
                MenuItem newMI = new MenuItem(price, name);

                int row = that.table.getSelectedRow();
                int col = that.table.getSelectedColumn();
                String menuItemToEdit = that.table.getValueAt(row, col).toString();
                if (name != "")
                    that.editMenuItem(new MenuItem(0, menuItemToEdit), newMI);
                else
                    JOptionPane.showMessageDialog(null, "Please select product name to be deleted");
            }
        });

        this.deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int row = that.table.getSelectedRow();
                int col = that.table.getSelectedColumn();
                String name = that.table.getValueAt(row, col).toString();
                if (name != "")
                    that.deleteMenuItem(new MenuItem(0, name));
                else
                    JOptionPane.showMessageDialog(null, "Please select product name to be deleted");
            }
        });
    }

    public void setFrameVisibility(boolean visibility) {
        this.frame.setVisible(visibility);
    }

    @Override
    public void createMenuItem(MenuItem menuItem) {
        Restaurant.getInstance().createMenuItem(menuItem);
        TableService.populateTable(this.tableModel, Restaurant.getInstance().getRestaurantAvailableMenuItems());
    }

    @Override
    public void deleteMenuItem(MenuItem menuItem) {
        Restaurant.getInstance().deleteMenuItem(menuItem);
        TableService.populateTable(this.tableModel, Restaurant.getInstance().getRestaurantAvailableMenuItems());
    }

    @Override
    public void editMenuItem(MenuItem old, MenuItem newMenuItem) {
        Restaurant.getInstance().editMenuItem(old, newMenuItem);
        TableService.populateTable(this.tableModel, Restaurant.getInstance().getRestaurantAvailableMenuItems());
    }

    @Override
    public void createNewOrder(int table, MenuItem orderedItem) {

    }

    @Override
    public void computeBill(int table) {

    }

    @Override
    public boolean isWellFormed() {
        return false;
    }
}
