package presentationLayer;

import businessLayer.MenuItem;
import businessLayer.Restaurant;
import services.ResourceService;
import services.TableService;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

public class ChefGraphicalUserInterface implements Observer {
    private JTable ordersTable;
    private DefaultTableModel ordersTableModel;

    private JFrame frame;

    public ChefGraphicalUserInterface() {
        this.frame = new JFrame("Chef");

        // create the little table for orders
        JPanel tablePanelL = new JPanel();
        tablePanelL.setLayout(new GridLayout(1, 1));
        this.ordersTable = new JTable();
        this.ordersTableModel = new DefaultTableModel();
        ordersTableModel.addColumn("Product");
        this.ordersTable.setModel(ordersTableModel);
        // populate model
        TableService.populateTable(this.ordersTableModel, Restaurant.getInstance().getOrdersList());
        JScrollPane scrollTableL = new JScrollPane(this.ordersTable);
        scrollTableL.setLayout(new ScrollPaneLayout());
        scrollTableL.setVisible(true);
        tablePanelL.add(scrollTableL);

        // add panels to main panel
        Container mainPanel = frame.getContentPane();
        mainPanel.setLayout(new GridLayout(1, 1));
        mainPanel.add(tablePanelL);
        frame.setSize(700, 700);
        frame.pack();
    }

    public void setFrameVisibility(boolean visibility) {
        this.frame.setVisible(visibility);
    }

    @Override
    public void update(Observable observable, Object obj) {
        System.out.println(obj);
        Vector<String> r = new Vector<>();
        r.addElement(((MenuItem) obj).getName());
        ordersTableModel.addRow(r);
    }
}
