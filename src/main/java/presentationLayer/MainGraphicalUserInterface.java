package presentationLayer;

import businessLayer.Restaurant;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainGraphicalUserInterface {
    private JButton adminButton;
    private JButton waiterButton;
    private JButton chefButton;

    private JFrame frame;

    private AdministratorGraphicalUserInterface adminUI;
    private WaiterGraphicalUserInterface waiterUI;
    private ChefGraphicalUserInterface chefUI;

    public MainGraphicalUserInterface() {
        this.adminUI = new AdministratorGraphicalUserInterface();
        this.waiterUI = new WaiterGraphicalUserInterface();
        this.chefUI = new ChefGraphicalUserInterface();
        Restaurant.getInstance().addObserver(chefUI);

        this.frame = new JFrame("Main");
        frame.setSize(230, 170);

        this.adminButton = new JButton("Admininstrator");
        this.waiterButton = new JButton("Waiter");
        this.chefButton = new JButton("Chef");

        JPanel panel = new JPanel(new GridLayout(3, 1));
        panel.add(adminButton);
        panel.add(waiterButton);
        panel.add(chefButton);

        frame.add(panel);
        frame.setLayout(new GridLayout(1, 1));
        frame.setVisible(true);


        this.registerActionListener();
    }

    private void registerActionListener() {
        final MainGraphicalUserInterface that = this;

        this.adminButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                that.adminUI.setFrameVisibility(true);
            }
        });

        this.waiterButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                that.waiterUI.setFrameVisibility(true);
            }
        });

        this.chefButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                that.chefUI.setFrameVisibility(true);
            }
        });
    }


}
