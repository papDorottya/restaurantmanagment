package presentationLayer;

import businessLayer.*;
import businessLayer.MenuItem;
import dataLayer.RestaurantSerializator;
import services.ResourceService;
import services.TableService;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class WaiterGraphicalUserInterface implements IRestaurantProcessing {
    private JLabel tableNumberLabel;
    private JTextField tabelNumberText;

    private JLabel computedPriceLabel;
    private JLabel priceLabel;

    private JButton addButton;
    private JButton computeTotalPrice;
    private JButton computeBillButton;

    private JTable table;
    private DefaultTableModel tableModel;

    private JTable ordersTable;
    private DefaultTableModel ordersTableModel;

    private JFrame frame;

    private RestaurantSerializator restaurantSerializator = new RestaurantSerializator(ResourceService.getInstance().getFileName());

    public WaiterGraphicalUserInterface() {
        this.frame = new JFrame("Waiter");

        //create the little table for orders
        JPanel tablePanelL = new JPanel();
        tablePanelL.setLayout(new GridLayout(1, 1));
        this.ordersTable = new JTable();
        this.ordersTableModel = new DefaultTableModel();
        ordersTableModel.addColumn("Added product");
        ordersTableModel.addColumn("Price");
        this.ordersTable.setModel(ordersTableModel);
        JScrollPane scrollTableL = new JScrollPane(this.ordersTable);
        scrollTableL.setLayout(new ScrollPaneLayout());
        scrollTableL.setVisible(true);
        tablePanelL.add(scrollTableL);

        // create control panel
        JPanel controlsPanel = new JPanel();
        controlsPanel.setLayout(new GridLayout(3, 1));
        //tabel number
        JPanel tableNumberPanel = new JPanel();
        tableNumberPanel.setLayout(new GridLayout(1, 2));
        this.tableNumberLabel = new JLabel("Tabel number");
        this.tabelNumberText = new JTextField("");
        tableNumberPanel.add(this.tableNumberLabel);
        tableNumberPanel.add(this.tabelNumberText);
        controlsPanel.add(tableNumberPanel);
        //compute price
        JPanel pricePanel = new JPanel();
        pricePanel.setLayout(new GridLayout(1, 2));
        this.computedPriceLabel = new JLabel("Computed price");
        this.priceLabel = new JLabel("");
        pricePanel.add(this.computedPriceLabel);
        pricePanel.add(this.priceLabel);
        controlsPanel.add(pricePanel);
        //buttons
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(3, 1));
        this.computeTotalPrice = new JButton("Compute total price");
        this.addButton = new JButton("<--- Add product");
        this.computeBillButton = new JButton("Compute bill");
        buttonsPanel.add(this.computeTotalPrice);
        buttonsPanel.add(this.addButton);
        buttonsPanel.add(this.computeBillButton);
        controlsPanel.add(buttonsPanel);

        //create table panel
        JPanel tablePanel = new JPanel();
        tablePanel.setLayout(new GridLayout(1, 1));
        this.table = new JTable();
        this.tableModel = new DefaultTableModel();
        tableModel.addColumn("Name");
        tableModel.addColumn("Price");
        TableService.populateTable(this.tableModel, Restaurant.getInstance().getRestaurantAvailableMenuItems());
        this.table.setModel(tableModel);
        JScrollPane scrollTable = new JScrollPane(this.table);
        scrollTable.setLayout(new ScrollPaneLayout());
        scrollTable.setVisible(true);
        tablePanel.add(scrollTable);

        // add panels to main panel
        Container mainPanel = frame.getContentPane();
        mainPanel.setLayout(new GridLayout(1, 3));
        mainPanel.add(tablePanelL);
        mainPanel.add(controlsPanel);
        mainPanel.add(tablePanel);
        frame.setSize(700, 700);
        frame.pack();

        this.registerActionListeners();
    }

    private void registerActionListeners() {
        final WaiterGraphicalUserInterface that = this;
        this.addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int tableNumber = Integer.parseInt(that.tabelNumberText.getText());

                int row = that.table.getSelectedRow();
                int col = that.table.getSelectedColumn();
                String menuItemToAdd = that.table.getValueAt(row, col).toString();
                int priceToAdd = Integer.parseInt((String) that.table.getValueAt(row, col + 1));
                if (menuItemToAdd != "")
                    that.createNewOrder(tableNumber, new MenuItem(priceToAdd, menuItemToAdd));
                else
                    JOptionPane.showMessageDialog(null, "Please select product name to be added");
            }
        });

        this.computeTotalPrice.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String table = that.tabelNumberText.getText();

                if (table != null) {
                    int tableNumber = Integer.parseInt(table);
                    that.priceLabel.setText(Restaurant.getInstance().computeTotalPrice(tableNumber));
                } else
                    JOptionPane.showMessageDialog(null, "Please enter the table number");
            }
        });

        this.computeBillButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String table = that.tabelNumberText.getText();

                if (table != "") {
                    int tableNumber = Integer.parseInt(table);
                    that.computeBill(tableNumber);
                } else
                    JOptionPane.showMessageDialog(null, "Please enter the table number");
            }
        });
    }

    public void setFrameVisibility(boolean visibility) {
        this.frame.setVisible(visibility);
    }

    @Override
    public void createMenuItem(MenuItem menuItem) {

    }

    @Override
    public void deleteMenuItem(MenuItem menuItem) {

    }

    @Override
    public void editMenuItem(MenuItem old, MenuItem newMenuItem) {

    }

    @Override
    public void createNewOrder(int table, MenuItem orderedItem) {
        Restaurant.getInstance().createNewOrder(table, orderedItem);
        TableService.populateTable(this.ordersTableModel, Restaurant.getInstance().getTableMenuItems(table));
    }

    @Override
    public void computeBill(int table) {
        Restaurant.getInstance().computeBill(table);
    }

    @Override
    public boolean isWellFormed() {
        return false;
    }
}
