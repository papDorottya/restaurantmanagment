package services;

public class ResourceService {
    private static ResourceService singleInstance = null;

    private String fileName;

    private ResourceService() {
    }

    public static ResourceService getInstance() {
        if(singleInstance == null) {
            singleInstance = new ResourceService();
        }

        return  singleInstance;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
