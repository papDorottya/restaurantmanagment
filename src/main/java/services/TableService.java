package services;

import businessLayer.MenuItem;

import javax.swing.table.DefaultTableModel;
import java.util.List;
import java.util.Vector;

public class TableService {
    public static void populateTable(DefaultTableModel table, List<MenuItem> menuItems) {
        if (table.getRowCount() > 0) {
            for (int i = table.getRowCount() - 1; i > -1; i--) {
                table.removeRow(i);
            }
        }
        for (MenuItem mi : menuItems) {
            Vector<String> r = new Vector<>();
            r.addElement(mi.getName());
            r.addElement(String.valueOf(mi.getPrice()));
            table.addRow(r);
        }
    }
}
